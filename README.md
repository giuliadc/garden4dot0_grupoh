<h1>Garden 4.0</h1>

<h2>  Estrutura de arquivos</h2>
<p align="center">
  <img src="estrutura.png" width="500" title="Estrutura">
</p>
<hr>
<h2> Classes e Interfaces</h2>
<p align="center">
  <img src="classes_interfaces.png" width="500" title="Interfaces">
  <img src="classes_interfaces_codigo.png" width="500" title="Interfaces">
</p>
<h2> Máquina de Estados</h2>
<p align="center">
  <img src="maquinaestados.jpeg" width="500" title="Máquina de Estados">
</p>
<h2> Manual</h2>
<p align="center">
  <img src="manual.png" width="500" title="Manual">
</p>
