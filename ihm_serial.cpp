#include <Arduino.h>
#include <SoftwareSerial.h>

#include "definicoes_sistema.h"
#include "ihm_serial.h"

InterfaceHomemMaquinaSerial::InterfaceHomemMaquinaSerial()
{
}

void InterfaceHomemMaquinaSerial::begin(int bluetoothPin1, int bluetoothPin2){
    pin1 = bluetoothPin1;
    pin2 = bluetoothPin2;
    SoftwareSerial mySerial(pin1, pin2); // RX, TX  
    mySerial.begin(38400); 
}

/************************
 InterfaceHomemMaquinaSerial::exibirMsg
 Exibe mensagem no display
 entradas
   texto : texto a ser validado como senha
 saidas
   nenhuma
*************************/
void InterfaceHomemMaquinaSerial::exibirMsg(char* texto)
{
    Serial.print("Mensagem para display: ");
    Serial.println(texto);
}

/************************
 InterfaceHomemMaquinaSerial::obterTecla
 Obtem tecla do teclado
 entradas
   nenhuma
 saidas
   teclas lidas do teclado
*************************/
char* InterfaceHomemMaquinaSerial::obterTeclas()
{
  // Serial.print("obter teclas:");
  int read_count = 0;
  
  // check for input
  if (mySerial.available() > 0) {
    // read the incoming bytes:
    read_count = mySerial.readBytesUntil('\n',buf, sizeof(buf)/sizeof(buf[0]) - 1);
  }
  
  buf[read_count] = '\0';
  if(read_count > 0) {
  	mySerial.println(buf);
  }
  
  return buf;
}
