#ifndef HUMSENSOR_H_INCLUDED
#define HUMSENSOR_H_INCLUDED


class HumSensorInterface {

  public: 
    virtual void begin(int humPin) = 0;
    virtual void ativar(int controle) = 0;
    virtual int read() = 0;
};

#endif // HUMSENSOR_H_INCLUDED