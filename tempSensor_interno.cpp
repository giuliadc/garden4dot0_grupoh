#include <Arduino.h>
#include <OneWire.h> //INCLUSÃO DE BIBLIOTECA
#include <DallasTemperature.h> //INCLUSÃO DE BIBLIOTECA

#include "definicoes_sistema.h"
#include "tempSensor_interno.h"

TempSensor::TempSensor(){
}

void TempSensor::begin(int tempPin) {

  pin = tempPin;

  OneWire ourWire(pin); //CONFIGURA UMA INSTÂNCIA ONEWIRE PARA SE COMUNICAR COM O SENSOR
  DallasTemperature sensors(&ourWire); //BIBLIOTECA DallasTemperature UTILIZA A OneWire
  sensors.begin();

}



void TempSensor::ativar(int controle) {

  ativo = controle;

}



int TempSensor::read() {

  sensors.requestTemperatures();//SOLICITA QUE A FUNÇÃO INFORME A TEMPERATURA DO SENSOR
  Serial.print("Temperatura: "); //IMPRIME O TEXTO NA SERIAL
  Serial.print(sensors.getTempCByIndex(0)); //IMPRIME NA SERIAL O VALOR DE TEMPERATURA MEDIDO
  Serial.println("*C"); //IMPRIME O TEXTO NA SERIAL

  return sensors.getTempCByIndex(0);

}