#ifndef CONDIDEAL_INTERNO_H_INCLUDED
#define CONDIDEAL_INTERNO_H_INCLUDED

#include "humSensor.h"
#include "definicoes_sistema.h"

class PlantaIdeal{
    public:
        PlantaIdeal();
        void editar(int tecla, int valor);
        int umidadeSolo;
        int umidadeAr;
        int temperaturaAr;
        int temperaturaSolo;
};

#endif // CONDIDEAL_INTERNO_H_INCLUDED