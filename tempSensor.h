#ifndef TEMPSENSOR_H_INCLUDED
#define TEMPSENSOR_H_INCLUDED


class TempSensorInterface {

  public: 
    virtual void begin(int tempPin) = 0;
    virtual void ativar(int controle) = 0;
    virtual void read() = 0;
};

#endif // TEMPSENSOR_H_INCLUDED