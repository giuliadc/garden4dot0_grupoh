#ifndef AIRSENSOR_H_INCLUDED
#define AIRSENSOR_H_INCLUDED


class AirSensorInterface {

  public: 
    virtual void begin(int airPin) = 0;
    virtual void ativar(int controle) = 0;
    virtual void read() = 0;
};

#endif // AIRSENSOR_H_INCLUDED