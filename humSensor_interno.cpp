#include <Arduino.h>

#include "definicoes_sistema.h"
#include "humSensor_interno.h"

HumSensor::HumSensor(){
}

void HumSensor::begin(int humPin) {

  pin = humPin;

  pinMode(pin, INPUT);

}



void HumSensor::ativar(int controle) {

  ativo = controle;

}



int HumSensor::read() {
  
  int umidade = analogRead(pin);
  int umidadeCor = map(umidade, 0, 1023, 100, 0); 

  return umidadeCor;

}
