#include <Arduino.h>

#include "definicoes_sistema.h"
#include "valvula_interno.h"

Valvula::Valvula(){
}

void Valvula::begin(int valvulaPin) {

  pin = valvulaPin;

  pinMode(pin, OUTPUT);

}



void Valvula::acionar(int controle) {

  ativo = controle;
  if (controle == TRUE){
    digitalWrite(pin,LOW);
  } else {
    digitalWrite(pin,HIGH);
  }
  
}

