#ifndef VALVULA_H_INCLUDED
#define AIRSENSOR_H_INCLUDED


class ValvulaInterface {

  public: 
    virtual void begin(int valvulaPin) = 0;
    virtual void acionar(int controle) = 0;
};

#endif // VALVULA_H_INCLUDED