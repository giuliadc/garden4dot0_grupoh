#ifndef DEFINICOES_SISTEMA_H_INCLUDED
#define DEFINICOES_SISTEMA_H_INCLUDED

#define true  1
#define false 0

#define TRUE  1
#define FALSE 0

#define NUM_ESTADOS 2
#define NUM_EVENTOS 4

// ESTADOS
#define IDLE   0
#define REGA    1

// EVENTOS
#define NENHUM_EVENTO -1
#define LI             0
#define START          1
#define TIMEOUT        2
#define EDICAO         3

// ACOES
#define NENHUMA_ACAO -1
#define A01  0
#define A02  1
#define A03  2


#endif // DEFINICOES_SISTEMA_H_INCLUDED
