#ifndef TEMPSENSOR_INTERNO_H_INCLUDED
#define TEMPSENSOR_INTERNO_H_INCLUDED

#include "tempSensor.h"
#include "definicoes_sistema.h"

class TempSensor: public TempSensorInterface{
    public:
        TempSensor();
        void begin(int tempPin);
        void ativar(int controle) = 0;
        void read() = 0;

    private: 
        int pin;
        int ativo = FALSE;
};

#endif // TEMPSENSOR_INTERNO_H_INCLUDED