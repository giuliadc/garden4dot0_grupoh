#ifndef HUMSENSOR_INTERNO_H_INCLUDED
#define HUMSENSOR_INTERNO_H_INCLUDED

#include "humSensor.h"
#include "definicoes_sistema.h"

class HumSensor: public HumSensorInterface{
    public:
        HumSensor();
        void begin(int humPin);
        void ativar(int controle) = 0;
        void int() = 0;

    private: 
        int pin;
        int ativo = FALSE;
};

#endif // HUMSENSOR_INTERNO_H_INCLUDED