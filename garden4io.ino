/*
    GARDEN4.0
*/

#include "definicoes_sistema.h"
#include "comunicacao_serial.h"
#include "ihm_serial.h"
#include "timer_interno.h"
#include "valvula_interno.h"
#include "humSensor_interno.h"
#include "tempSensor_interno.h"
#include "airSensor_interno.h"
#include "condicoesIdeais_interno.h"
#include <string.h>

/***********************************************************************
 Componentes
 ***********************************************************************/
ComunicacaoSerial com;
Valvula valvula;
AirSensor airSensor;
TempSensor tempSensor;
HumSensor humSensor;
PlantaIdeal plantaIdeal;
InterfaceHomemMaquinaSerial ihm;
TimerInterno tmr;
TimerInterno tmrProxima;

/***********************************************************************
 Estaticos
 ***********************************************************************/
int codigoEvento = NENHUM_EVENTO;
int eventoInterno = NENHUM_EVENTO;
int estado = IDLE;
int codigoAcao;
int acao_matrizTransicaoEstados[NUM_ESTADOS][NUM_EVENTOS];
int proximo_estado_matrizTransicaoEstados[NUM_ESTADOS][NUM_EVENTOS];
const int tempPin = A0;
const int humPin = A1;
const int airPin = A2;
const int valvulaPin = 4;
const int bluetoothPin1 = 10;
const int bluetoothPin2 = 11;

/************************************************************************
 executarAcao
 Executa uma acao
 Parametros de entrada:
    (int) codigo da acao a ser executada
 Retorno: (int) codigo do evento interno ou NENHUM_EVENTO
*************************************************************************/
int executarAcao(int codigoAcao)
{
    int retval;

    retval = NENHUM_EVENTO;
    if (codigoAcao == NENHUMA_ACAO)
        return retval;

    switch(codigoAcao)
    {
    case A01:
        tmr.iniciar(true);
        valvula.acionar(TRUE);
        break;
    case A02:
        valvula.acionar(FALSE);
        com.notificar("Irrigação finalizada");
        tmr.iniciar(false);
        break;
    case A03:
        teclas = ihm.obterTeclas();
        char valor = atof(ihm.obterTeclas());
        if (teclas[0] == 'a') {
            if (teclas[1] == 'u') {
              char tecla = 'au';
              plantaIdeal.editar(tecla, valor);
            }
            else{
              char tecla = 'at';
              plantaIdeal.editar(tecla, valor);
            }
        }
        else{
          plantaIdeal.editar(teclas[0], valor);
        }
        
    } // switch

    return retval;
} // executarAcao

/************************************************************************
 iniciaMaquina de Estados
 Carrega a maquina de estados
 Parametros de entrada: nenhum
 Retorno: nenhum
*************************************************************************/

void iniciaMaquinaEstados()
{
  int i;
  int j;

  for (i=0; i < NUM_ESTADOS; i++) {
    for (j=0; j < NUM_EVENTOS; j++) {
       acao_matrizTransicaoEstados[i][j] = NENHUMA_ACAO;
       proximo_estado_matrizTransicaoEstados[i][j] = i;
    }
  }

  proximo_estado_matrizTransicaoEstados[IDLE][LI] = REGA;
  acao_matrizTransicaoEstados[IDLE][LI] = A01;

  proximo_estado_matrizTransicaoEstados[IDLE][START] = REGA;
  acao_matrizTransicaoEstados[IDLE][START] = A01;

  proximo_estado_matrizTransicaoEstados[REGA][TIMEOUT] = IDLE;
  acao_matrizTransicaoEstados[REGA][TIMEOUT] = A02;

  proximo_estado_matrizTransicaoEstados[IDLE][EDICAO] = IDLE;
  acao_matrizTransicaoEstados[IDLE][EDICAO] = A03;

} // initStateMachine

/************************************************************************
 iniciaSistema
 Inicia o sistema ...
 Parametros de entrada: nenhum
 Retorno: nenhum
*************************************************************************/
void iniciaSistema()
{
   iniciaMaquinaEstados();
} // initSystem


/************************************************************************
 obterEvento
 Obtem um evento, que pode ser da IHM ou do alarme
 Parametros de entrada: nenhum
 Retorno: codigo do evento
*************************************************************************/
char* teclas;

int decodificarEditar()
{
    if (teclas[0] == 'e')
    {
        return true;
    }
    return false;
}//decodificarEditar

int decodificarAcionar()
{
    if (teclas[0] == 'l')
    {
        if (valvula.ativo == FALSE)
        {
            tmrProxima.iniciar(TRUE);
            return true;
        }
    }
    tmrProxima.iniciar(FALSE);
    return false;
}//decodificarAcionar

int decodificarDisparar(int humRead)
{
    if ( humRead < plantaIdeal.umidadeSolo && tmrProxima.timeout())
    {
        tmrProxima.iniciar(TRUE);
        return true;
    }
    tmrProxima.iniciar(FALSE);
    return false;
}//decodificarDisparar

int decodificarTimeout()
{
    return tmr.timeout();
}

int obterEvento()
{
  int retval = NENHUM_EVENTO;
  int humRead = humSensor.read();

  teclas = ihm.obterTeclas();
  airSensor.read();
  tempSensor.read();
  
  if (decodificarAcionar())
    return LI;
  if (decodificarDisparar(humRead))
    return START;
  if (decodificarTimeout())
    return TIMEOUT;
  if (decodificarEditar())
    return EDICAO;

  return retval;

} // obterEvento

/************************************************************************
 obterAcao
 Obtem uma acao da Matriz de transicao de estados
 Parametros de entrada: estado (int)
                        evento (int)
 Retorno: codigo da acao
*************************************************************************/
int obterAcao(int estado, int codigoEvento) {
  return acao_matrizTransicaoEstados[estado][codigoEvento];
} // obterAcao


/************************************************************************
 obterProximoEstado
 Obtem o proximo estado da Matriz de transicao de estados
 Parametros de entrada: estado (int)
                        evento (int)
 Retorno: codigo do estado
*************************************************************************/
int obterProximoEstado(int estado, int codigoEvento) {
  return proximo_estado_matrizTransicaoEstados[estado][codigoEvento];
} // obterAcao


/************************************************************************
 Main
 Loop principal de controle que executa a maquina de estados
 Parametros de entrada: nenhum
 Retorno: nenhum
*************************************************************************/

void setup() {
  Serial.begin(9600);

  iniciaSistema();
  airSensor.begin(airPin);
  tempSensor.begin(tempPin);
  ihm.begin(bluetoothPin1, bluetoothPin2);
  humSensor.begin(humPin);
  valvula.begin(valvulaPin);
  Serial.println("Garden 4.0 iniciado");

} // setup

void loop() {
  if (eventoInterno == NENHUM_EVENTO) {
      codigoEvento = obterEvento();
  } else {
      codigoEvento = eventoInterno;
  }
  if (codigoEvento != NENHUM_EVENTO)
  {
      codigoAcao = obterAcao(estado, codigoEvento);
      estado = obterProximoEstado(estado, codigoEvento);
      eventoInterno = executarAcao(codigoAcao);
      Serial.print("Estado: ");
      Serial.print(estado);
      Serial.print(" Evento: ");
      Serial.print(codigoEvento);
      Serial.print(" Acao: ");
      Serial.println(codigoAcao);
  }
} // loop
