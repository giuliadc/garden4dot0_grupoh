#include <Arduino.h>

#include "definicoes_sistema.h"
#include "airSensor_interno.h"
#include "DHT.h"
 
#define dht_pin A5 //Pino DATA do Sensor ligado na porta Analogica A5
 
#define DHTTYPE DHT11
 


AirSensor::AirSensor(){
}

void AirSensor::begin(int airPin) {

  pin = airPin;

  DHT dht(pin, DHTTYPE);

  dht.begin();

}



void AirSensor::ativar(int controle) {

  ativo = controle;

}



int AirSensor::read() {

  float h = dht.readHumidity();
  float t = dht.readTemperature();
 
  // Mostra os valores lidos, na serial
  Serial.print("Temp. Air = ");
  Serial.print(t);
  Serial.print(" C ");
  Serial.print("Um. Air = ");
  Serial.print(h);
  Serial.println(" % ");
 
  return h, t

}

