#ifndef AIRSENSOR_INTERNO_H_INCLUDED
#define AIRSENSOR_INTERNO_H_INCLUDED

#include "airSensor.h"
#include "definicoes_sistema.h"

class AirSensor: public AirSensorInterface{
    public:
        AirSensor();
        void begin(int airPin);
        void ativar(int controle) = 0;
        void read() = 0;

    private: 
        int pin;
        int ativo = FALSE;
};

#endif // AIRSENSOR_INTERNO_H_INCLUDED