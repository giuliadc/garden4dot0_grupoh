#ifndef IHM_SERIAL_H_INCLUDED
#define IHM_SERIAL_H_INCLUDED

#include "ihm.h"

#define NENHUMA_TECLA -1

class InterfaceHomemMaquinaSerial: public InterfaceHomemMaquina {
  public:
  InterfaceHomemMaquinaSerial();
  void begin(int bluetoothPin1, int bluetoothPin2);
  void exibirMsg(char* texto);
  char* obterTeclas();

  private:
  char buf[10];
  int pin1;
  int pin2;
};

#endif // IHM_SERIAL_H_INCLUDED
