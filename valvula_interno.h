#ifndef VALVULA_INTERNO_H_INCLUDED
#define VALVULA_INTERNO_H_INCLUDED

#include "valvula.h"
#include "definicoes_sistema.h"

class Valvula: public ValvulaInterface{
    public:
        Valvula();
        void begin(int valvulaPin);
        void acionar(int controle) = 0;
        int ativo = FALSE;

    private: 
        int pin;
};

#endif // VALVULA_INTERNO_H_INCLUDED