// Librarys.
#include <OneWire.h> //INCLUSÃO DE BIBLIOTECA
#include <DallasTemperature.h> //INCLUSÃO DE BIBLIOTECA
#include <DHT.h>
#include <Adafruit_Sensor.h>
#include "SoftwareSerial.h"

// Defines
#define DS18B20 7 //Pino digital para o sensor de temperatura da terra
#define DHTPIN 8//Pino digital 8 conectado ao DHT11(sensor do ar)
#define DHTTYPE DHT11 //DHT 11 - Especifica qual tipo de DHT

// Variaveis Globais
const int humPin = A1;
const int valvulaPin = 4;
const int bluetoothPin1 = 10;
const int bluetoothPin2 = 11;
char incomingByte;

int limUmidade = 20; //limiar de umidade necessaria para regar a planta. 0 a 100, recomenda-se valores de 20 a 30.
 
unsigned long period = 18000;  //modifique aqui o intervalo entre medidas (ms).   
unsigned long time_now = 0;  // nao modifique este valor.

// Objetos
DHT dht(DHTPIN, DHTTYPE);
OneWire ourWire(DS18B20); //CONFIGURA UMA INSTÂNCIA ONEWIRE PARA SE COMUNICAR COM O SENSOR
DallasTemperature sensors(&ourWire); //BIBLIOTECA DallasTemperature UTILIZA A OneWire
SoftwareSerial bluetooth(bluetoothPin1,bluetoothPin2);

// Inicialização 
void setup() {
  Serial.begin(9600);
  bluetooth.begin(9600);
  dht.begin();//Inicializa o sensor DHT11
  sensors.begin();
  Serial.println("DHTxx test!");
  pinMode(valvulaPin, OUTPUT); //saida para acionar bomba da agua,
  pinMode(humPin, INPUT);
  delay(1000);
}

//loop infinito
void loop() {

  // Variáveis locais
  int umidade = analogRead(humPin);
  int umidadeCor = map(umidade,0,1023,100,0);
  
  float h = dht.readHumidity();//lê o valor da umidade e armazena na variável h do tipo float (aceita números com casas decimais)
  float t = dht.readTemperature();//lê o valor da temperatura e armazena na variável t do tipo float (aceita números com casas decimais)int umidade = analogRead(humPin);
  sensors.requestTemperatures();//SOLICITA QUE A FUNÇÃO INFORME A TEMPERATURA DO SENSOR

  // Sensor do ar
  Serial.print("Umidade do ar: ");//Imprime no monitor serial a mensagem "Umidade: "
  Serial.print(h);//Imprime na serial o valor da umidade
  Serial.println("%");//Imprime na serial o caractere "%" e salta para a próxima linha
  delay(250);
  Serial.print("Temperatura do ar: ");//Imprime no monitor serial a mensagem "Temperatura: "
  Serial.print(t);//Imprime na serial o valor da temperatura
  Serial.println("°C ");//Imprime no monitor serial "ºC" e salta para a próxima linha
  delay(250);

  // Umidade da terra
  Serial.print("Umidade da terra: ");//Imprime no monitor serial a mensagem "Umidade: "
  Serial.print(umidadeCor);//Imprime na serial o valor da umidade
  Serial.println("%");//Imprime na serial o caractere "%" e salta para a próxima linha
  delay(250);
  
  // Sensor Temperatura da terra
  Serial.print("Temperatura da terra: "); //IMPRIME O TEXTO NA SERIAL
  Serial.print(sensors.getTempCByIndex(0)); //IMPRIME NA SERIAL O VALOR DE TEMPERATURA MEDIDO
  Serial.println("°C"); //IMPRIME O TEXTO NA SERIAL
  delay(250);//INTERVALO DE 250 MILISSEGUNDOS

  // Sensor Temperatura do Solo
  bluetooth.print("Temperatura do Solo: "); //IMPRIME O TEXTO NA SERIAL
  bluetooth.print(sensors.getTempCByIndex(0)); //IMPRIME NA SERIAL O VALOR DE TEMPERATURA MEDIDO
  bluetooth.println("°C");
  
  bluetooth.print("Umidade do Solo:");
  bluetooth.print(umidadeCor);
  bluetooth.println("%");
  
  bluetooth.print("Temp. Air = ");
  bluetooth.print(t);
  bluetooth.println("°C ");
  
  bluetooth.print("Umidade do Air = ");
  bluetooth.print(h);
  bluetooth.println(" % ");
    
  if (bluetooth.available() > 0) {
    // read the oldest byte in the serial buffer:
    incomingByte = bluetooth.read();
    if (incomingByte == 'l') {
      digitalWrite(valvulaPin, LOW);
      bluetooth.println("REGANDO, BOMBA LIGADA!");
      delay(10000);
      digitalWrite(valvulaPin,HIGH);
    }
    if (millis() >= time_now + period) {
      time_now += period;
    
      if(umidade<=limUmidade){
        digitalWrite(valvulaPin,LOW);
        Serial.println("REGANDO, BOMBA LIGADA!");
        delay(10000);
        digitalWrite(valvulaPin,HIGH);
      }
    }
  }
  bluetooth.println("==============================");
  delay(5000);
}

//FOntes
/* 
 *  Bluetooth https://www.robocore.net/tutoriais/bluetooth-hc-05-com-arduino-comunicando-com-pc
 *  Umidade Solo https://blog.moduloeletronica.com.br/5-2/
 *  DHT11 https://www.filipeflop.com/blog/monitorando-temperatura-e-umidade-com-o-sensor-dht11/
 *  Temperatura Solo https://blogmasterwalkershop.com.br/arduino/como-usar-com-arduino-sensor-de-temperatura-ds18b20-prova-dagua-do-tipo-sonda*/
